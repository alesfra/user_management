package com.cgi.alf.usermanagement.security;

import com.cgi.alf.usermanagement.SpringApplicationContext;
import com.cgi.alf.usermanagement.dtos.PersonDto;
import com.cgi.alf.usermanagement.models.PersonLoginRequestModel;
import com.cgi.alf.usermanagement.services.PersonService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    public AuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            PersonLoginRequestModel credentials = new ObjectMapper()
                    .readValue(req.getInputStream(), PersonLoginRequestModel.class);
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            credentials.getEmail(),
                            credentials.getPassword(),
                            new ArrayList<>()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        String userName = ((UserPrincipal) auth.getPrincipal()).getUsername();

        String token = Jwts.builder()
                .setSubject(userName)
                .setExpiration(new Date(System.currentTimeMillis() + SecuritySpecification.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecuritySpecification.getTokenSecret())
                .compact();
        PersonService personService = (PersonService) SpringApplicationContext.getBean("personServiceImpl");
        PersonDto personDto = personService.getPersonByEmail(userName);

        response.addHeader(SecuritySpecification.HEADER_STRING, SecuritySpecification.TOKEN_PREFIX + token);
        response.addHeader("UserID", personDto.getUserId());
    }
}