package com.cgi.alf.usermanagement.security;

import com.cgi.alf.usermanagement.entities.GroupEntity;
import com.cgi.alf.usermanagement.entities.PersonEntity;
import com.cgi.alf.usermanagement.entities.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserPrincipal implements UserDetails {

    private PersonEntity personEntity;
    private String userId;

    @Autowired
    public UserPrincipal(PersonEntity personEntity) {
        this.personEntity = personEntity;
        this.userId = personEntity.getUserId();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        List<GrantedAuthority> authorities = new ArrayList<>();
        List<GroupEntity> authorityEntities = new ArrayList<>();
        Collection<RoleEntity> roles = personEntity.getRoles();

        if (roles == null) {
            return authorities;
        } else {
            roles.forEach((role) -> {
                authorities.add(new SimpleGrantedAuthority(role.getType()));
                authorityEntities.addAll(role.getGroups());
            });

            authorityEntities.forEach((authorityEntity) -> {
                authorities.add(new SimpleGrantedAuthority(authorityEntity.getName()));
            });
        }

        return authorities;
    }

    @Override
    public String getPassword() {
        return this.personEntity.getEncryptedPassword();
    }

    @Override
    public String getUsername() {
        return this.personEntity.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}