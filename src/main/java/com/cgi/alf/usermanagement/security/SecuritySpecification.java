package com.cgi.alf.usermanagement.security;

import com.cgi.alf.usermanagement.SpringApplicationContext;

public class SecuritySpecification {

    public static final long EXPIRATION_TIME = 864000000; // token will be valid for 10 days in milisecs
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/persons";
    public static final String H2_CONSOLE = "/h2-console/**";

    public static String getTokenSecret() {
        AppProperties appProperties = (AppProperties) SpringApplicationContext.getBean("AppProperties");
        return appProperties.getTokenSecret();
    }
}