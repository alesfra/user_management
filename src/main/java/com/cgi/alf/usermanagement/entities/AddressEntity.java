package com.cgi.alf.usermanagement.entities;

import javax.persistence.*;
import java.io.Serializable;


@Entity(name = "addresses")
public class AddressEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String addressId;

    @Column(nullable = false, length = 150)
    private String city;

    @Column(nullable = false, length = 100)
    private String country;

    @Column(nullable = false, length = 250)
    private String street;

    @Column(nullable = false, length = 6)
    private String postcode;

    @ManyToOne
    @JoinColumn(name = "persons_id")
    private PersonEntity personDetails;

    public AddressEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public PersonEntity getPersonDetails() {
        return personDetails;
    }

    public void setPersonDetails(PersonEntity personDetails) {
        this.personDetails = personDetails;
    }
}