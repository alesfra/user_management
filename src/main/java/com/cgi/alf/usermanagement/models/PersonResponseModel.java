package com.cgi.alf.usermanagement.models;

import java.util.List;

public class PersonResponseModel {

    private String userId;
    private String name;
    private String email;
    private List<AddressesResponseModel> addresses;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<AddressesResponseModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressesResponseModel> addresses) {
        this.addresses = addresses;
    }
}