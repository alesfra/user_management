package com.cgi.alf.usermanagement.models;

import java.util.List;

public class PersonFullResponseModel {

    private String userId;
    private String name;
    private String email;
    private List<AddressesResponseModel> addresses;
    private List<RolesResponseModel> roles;
    private List<GroupsResponseModel> groups;

    public PersonFullResponseModel() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<AddressesResponseModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressesResponseModel> addresses) {
        this.addresses = addresses;
    }

    public List<RolesResponseModel> getRoles() {
        return roles;
    }

    public void setRoles(List<RolesResponseModel> roles) {
        this.roles = roles;
    }

    public List<GroupsResponseModel> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupsResponseModel> groups) {
        this.groups = groups;
    }
}