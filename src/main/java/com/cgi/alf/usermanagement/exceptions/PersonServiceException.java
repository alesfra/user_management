package com.cgi.alf.usermanagement.exceptions;

public class PersonServiceException extends RuntimeException {

    public PersonServiceException(String message) {
        super(message);
    }
}