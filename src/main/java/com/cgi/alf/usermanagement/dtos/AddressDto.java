package com.cgi.alf.usermanagement.dtos;

public class AddressDto {

    private Long id;
    private String addressId;
    private String city;
    private String country;
    private String street;
    private String postcode;
    private PersonDto personDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public PersonDto getPersonDetails() {
        return personDetails;
    }

    public void setPersonDetails(PersonDto personDetails) {
        this.personDetails = personDetails;
    }
}