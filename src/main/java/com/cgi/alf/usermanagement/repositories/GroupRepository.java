package com.cgi.alf.usermanagement.repositories;

import com.cgi.alf.usermanagement.entities.GroupEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends CrudRepository<GroupEntity, Long> {
    GroupEntity findByName(String name);
}