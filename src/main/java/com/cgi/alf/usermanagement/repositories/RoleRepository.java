package com.cgi.alf.usermanagement.repositories;

import com.cgi.alf.usermanagement.entities.RoleEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<RoleEntity, Long> {

    @Query("SELECT r FROM RoleEntity r JOIN r.users u WHERE u.id = :id")
    Optional<RoleEntity> findRolesByPersonId(@Param("id") Long id);

    RoleEntity findByType(String name);
}