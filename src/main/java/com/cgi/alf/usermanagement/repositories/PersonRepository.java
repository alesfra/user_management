package com.cgi.alf.usermanagement.repositories;

import com.cgi.alf.usermanagement.entities.PersonEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<PersonEntity, Long> {
    PersonEntity findUserByEmail(String email);

    PersonEntity findByUserId(String userId);
}