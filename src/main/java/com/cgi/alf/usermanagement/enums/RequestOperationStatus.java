package com.cgi.alf.usermanagement.enums;

public enum RequestOperationStatus {

    SUCCESS,
    ERROR
}