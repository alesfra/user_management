package com.cgi.alf.usermanagement.controllers;

import com.cgi.alf.usermanagement.dtos.PersonDto;
import com.cgi.alf.usermanagement.entities.RoleEntity;
import com.cgi.alf.usermanagement.enums.ErrorMessages;
import com.cgi.alf.usermanagement.enums.RequestOperationName;
import com.cgi.alf.usermanagement.enums.RequestOperationStatus;
import com.cgi.alf.usermanagement.exceptions.PersonServiceException;
import com.cgi.alf.usermanagement.models.*;
import com.cgi.alf.usermanagement.services.PersonService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("persons")
public class PersonController {

    PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }


    @PostMapping(
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )
    public PersonResponseModel createPerson(@RequestBody PersonDetailsRequestModel personDetails) throws Exception {

        PersonResponseModel returnValue = new PersonResponseModel();

        if (personDetails.getName().isEmpty()) {
            throw new PersonServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
        }

        ModelMapper modelMapper = new ModelMapper();
        PersonDto personDto = modelMapper.map(personDetails, PersonDto.class);

        PersonDto createdPerson = personService.createUser(personDto);
        returnValue = modelMapper.map(createdPerson, PersonResponseModel.class);
        return returnValue;
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or #id == principal.userId")
    @GetMapping(path = "/fullinfo/{id}")
    public PersonFullResponseModel getAllInfoById(@PathVariable("id") String id) {
        PersonFullResponseModel returnValue;
        PersonDto personDto = personService.findPersonById(id);
        ModelMapper modelMapper = new ModelMapper();
        returnValue = modelMapper.map(personDto, PersonFullResponseModel.class);
        return returnValue;
    }

    @GetMapping(path = "/via-email")
    public PersonResponseModel getPersonByEmail(@RequestParam("email") String email) {
        PersonResponseModel returnValue;
        PersonDto personDto = personService.getPersonByEmail(email);
        ModelMapper modelMapper = new ModelMapper();
        returnValue = modelMapper.map(personDto, PersonResponseModel.class);
        return returnValue;
    }

    @GetMapping(path = "/roles/{id}")
    public RolesResponseModel getRolesById(@PathVariable("id") Long id) {
        RolesResponseModel returnValue;
        Optional<RoleEntity> roleEntities = personService.findRoleByPersonId(id);
        ModelMapper modelMapper = new ModelMapper();
        returnValue = modelMapper.map(roleEntities, RolesResponseModel.class);
        return returnValue;
    }

    @GetMapping(path = "/{id}")
    public PersonResponseModel getPersonById(@PathVariable("id") String id) {
        PersonResponseModel returnValue;
        PersonDto personDto = personService.findPersonById(id);
        ModelMapper modelMapper = new ModelMapper();
        returnValue = modelMapper.map(personDto, PersonResponseModel.class);
        return returnValue;
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or #id == principal.userId")
    @DeleteMapping(path = "/{id}",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public OperationStatusModel deleteUser(@PathVariable String id) {

        OperationStatusModel returnValue = new OperationStatusModel();
        returnValue.setOperationName(RequestOperationName.DELETE.name());

        personService.deleteUser(id);

        returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
        return returnValue;
    }
}