package com.cgi.alf.usermanagement;

import com.cgi.alf.usermanagement.entities.GroupEntity;
import com.cgi.alf.usermanagement.entities.PersonEntity;
import com.cgi.alf.usermanagement.entities.RoleEntity;
import com.cgi.alf.usermanagement.repositories.GroupRepository;
import com.cgi.alf.usermanagement.repositories.PersonRepository;
import com.cgi.alf.usermanagement.repositories.RoleRepository;
import com.cgi.alf.usermanagement.utilities.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collection;

@Component
public class InitialPersonSetup {

    private RoleRepository roleRepository;
    private GroupRepository groupRepository;
    private Utils utils;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private PersonRepository personRepository;

    @Autowired
    public InitialPersonSetup(RoleRepository roleRepository, GroupRepository groupRepository,
                              Utils utils, BCryptPasswordEncoder bCryptPasswordEncoder, PersonRepository personRepository) {
        this.roleRepository = roleRepository;
        this.groupRepository = groupRepository;
        this.utils = utils;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.personRepository = personRepository;
    }

    @EventListener
    @Transactional
    public void onApplicationEvent(ApplicationReadyEvent event) {
        System.out.println(".....Application ready.....");

        GroupEntity readAuthority = createAuthority("READ AUTHORITY");
        GroupEntity writeAuthority = createAuthority("WRITE AUTHORITY");
        GroupEntity deleteAuthority = createAuthority("DELETE AUTHORITY");

        RoleEntity roleUser = createRole("ROLE_USER", Arrays.asList(readAuthority, writeAuthority));
        RoleEntity roleManager = createRole("ROLE_MANAGER", Arrays.asList(readAuthority, writeAuthority, deleteAuthority));

        if (roleManager == null) {
            return;
        }
        PersonEntity manager = new PersonEntity();
        manager.setName("Gordon Shumway");
        manager.setEmail("alf@melmac.org");
        manager.setSalary(1000000.00);
        manager.setUserId(utils.generateUserId(20));
        manager.setEncryptedPassword(bCryptPasswordEncoder.encode("12345"));
        manager.setRoles(Arrays.asList(roleManager));
        personRepository.save(manager);
    }

    @Transactional
    public GroupEntity createAuthority(String name) {
        GroupEntity groupEntity = groupRepository.findByName(name);
        if (groupEntity == null) {
            groupEntity = new GroupEntity(name);
            groupRepository.save(groupEntity);
        }
        return groupEntity;
    }

    @Transactional
    public RoleEntity createRole(String name, Collection<GroupEntity> groupOfAuthorities) {
        RoleEntity role = roleRepository.findByType(name);
        if (role == null) {
            role = new RoleEntity(name);
            role.setGroups(groupOfAuthorities);
            roleRepository.save(role);
        }
        return role;
    }
}