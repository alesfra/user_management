package com.cgi.alf.usermanagement.services;

import com.cgi.alf.usermanagement.dtos.PersonDto;
import com.cgi.alf.usermanagement.entities.RoleEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface PersonService extends UserDetailsService {
    PersonDto createUser(PersonDto person);

    PersonDto getPersonByEmail(String email);

    PersonDto findPersonById(String userId);

    PersonDto getUserByUserId(String userId);

    Optional<RoleEntity> findRoleByPersonId(Long id);

    void deleteUser(String userId);
}