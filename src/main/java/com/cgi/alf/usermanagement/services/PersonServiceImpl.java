package com.cgi.alf.usermanagement.services;

import com.cgi.alf.usermanagement.dtos.AddressDto;
import com.cgi.alf.usermanagement.dtos.PersonDto;
import com.cgi.alf.usermanagement.entities.PersonEntity;
import com.cgi.alf.usermanagement.entities.RoleEntity;
import com.cgi.alf.usermanagement.enums.ErrorMessages;
import com.cgi.alf.usermanagement.exceptions.PersonServiceException;
import com.cgi.alf.usermanagement.repositories.PersonRepository;
import com.cgi.alf.usermanagement.repositories.RoleRepository;
import com.cgi.alf.usermanagement.security.UserPrincipal;
import com.cgi.alf.usermanagement.utilities.Utils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    PersonRepository personRepository;
    Utils utils;
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository, Utils utils, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.personRepository = personRepository;
        this.utils = utils;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public PersonDto createUser(PersonDto person) {

        if (personRepository.findUserByEmail(person.getEmail()) != null) {
            throw new RuntimeException("Person with that email already exists");
        }

        for (int i = 0; i < person.getAddresses().size(); i++) {
            AddressDto address = person.getAddresses().get(i);
            address.setPersonDetails(person);
            address.setAddressId(utils.generateAddressId(15));
            person.getAddresses().set(i, address);
        }

        ModelMapper modelMapper = new ModelMapper();
        PersonEntity personEntity = modelMapper.map(person, PersonEntity.class);
        // BeanUtils.copyProperties(person, personEntity);

        String publicUserId = utils.generateUserId(20);
        personEntity.setUserId(publicUserId);
        personEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(person.getPassword()));
        personEntity.setSalary(person.getSalary());

        PersonEntity storedPersonDetails = personRepository.save(personEntity);
        //  BeanUtils.copyProperties(storedPersonDetails, returnValue);
        PersonDto returnValue = modelMapper.map(storedPersonDetails, PersonDto.class);
        return returnValue;
    }

    @Override
    public PersonDto getPersonByEmail(String email) {
        PersonEntity personEntity = personRepository.findUserByEmail(email);
        PersonDto returnValue = new PersonDto();
        BeanUtils.copyProperties(personEntity, returnValue);
        return returnValue;
    }

    @Override
    public PersonDto findPersonById(String userId) {

        PersonDto returnValue = new PersonDto();
        PersonEntity personEntity = personRepository.findByUserId(userId);

        if (personEntity == null) {
            throw new UsernameNotFoundException(userId);
        }
        BeanUtils.copyProperties(personEntity, returnValue);
        return returnValue;
    }

    public PersonDto getUserByUserId(String userId) {
        PersonDto returnValue = new PersonDto();
        PersonEntity userEntity = personRepository.findByUserId(userId);

        if (userEntity == null)
            throw new UsernameNotFoundException("User with ID: " + userId + " not found");

        BeanUtils.copyProperties(userEntity, returnValue);

        return returnValue;
    }

    public Optional<RoleEntity> findRoleByPersonId(Long id) {
        Optional<RoleEntity> roles = roleRepository.findRolesByPersonId(id);
        return roles;
    }

    @Override
    public void deleteUser(String userId) {
        PersonEntity personEntity = personRepository.findByUserId(userId);

        if (personEntity == null)
            throw new PersonServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        personRepository.delete(personEntity);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        PersonEntity personEntity = personRepository.findUserByEmail(email);

        if (personEntity == null) {
            throw new UsernameNotFoundException(email);
        }
        return new UserPrincipal(personEntity);
    }
}